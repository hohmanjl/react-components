const baseConfig = require('./.eslintrc');

baseConfig.plugins.push('only-warn');

module.exports = baseConfig;

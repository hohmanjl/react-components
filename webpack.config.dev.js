const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
const ProvidePlugin = require('webpack/lib/ProvidePlugin');
const NoEmitOnErrorsPlugin = require('webpack/lib/NoEmitOnErrorsPlugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const HotModuleReplacementPlugin = require('webpack/lib/HotModuleReplacementPlugin');

const ASSETS_PATH = 'css';

// Each instance will generate a separate resource
const extractTextAppStyles = new ExtractTextPlugin(`${ASSETS_PATH}/components.css`);
const extractTextVendorStyles = new ExtractTextPlugin(`${ASSETS_PATH}/vendors.css`);


const webpackConfig = {
  // main and vendor resources generated
  entry: {
    main: './src/demo/index.js',
    vendor: [
      'bootstrap',
      'jquery',
      'prop-types',
      'react',
      'react-dom',
      'react-redux',
      'redux'
    ]
  },

  resolve: {
    modules: ['node_modules', 'src'],
    extensions: ['.js']
  },

  // Exclude external testing stuff from bundles
  externals: {
    cheerio: 'window',
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },

  // this is generated "in memory" not on disk
  output: {
    path: path.join(__dirname, 'build'),
    filename: '[name].js',
    libraryTarget: 'umd',
    library: 'react-components',
    publicPath: '/'
  },

  plugins: [
    // Enable HMR
    new HotModuleReplacementPlugin(),

    // Set environment variables
    new DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    }),

    // Dynamically generate index.html and use the html-webpack-template for
    // app binding <div id="reactApp"></div>.
    new HtmlWebpackPlugin({
      inject: false,
      template: require('html-webpack-template'),
      appMountId: 'reactApp',
      title: '@jhohman/react-components development sandbox'
    }),

    // The external style sheets
    extractTextVendorStyles,
    extractTextAppStyles,

    // This enables bundle chunks, eg app.bundle.js, vendor.bundle.js
    new CommonsChunkPlugin({
      name: 'vendor',
      filename: 'js/vendors.js'
    }),

    // This is required so bootstrap can bootstrap. If not included,
    // bootstrap complains about missing jQuery.
    new ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery',
      jquery: 'jquery'
    }),

    new NoEmitOnErrorsPlugin()
  ],

  module: {
    rules: [
      {
        // compile the less via less-loader, pass result to css-loader
        // use sourcemaps for easy debugging
        test: /\.less$/,
        use: extractTextAppStyles.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'less-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        // compile css resources as vendor css with sourcemaps for debugging
        test: /\.css$/,
        use: extractTextVendorStyles.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        enforce: "pre",
        test: /\.js$/,
        exclude: [
          /node_modules/
        ],
        use: [
          {
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            loader: "source-map-loader"
          },
          {
            // compile javascript via eslint loader then pass result to babel loader
            loader: 'eslint-loader',
            options: {
              configFile: '.eslintrc.dev.js'
            }
          },
        ]
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          cacheDirectory: true,
        },
      },
      {
        // process small assets < 8192 bytes as data urls,
        // assets > 8192 bytes processed by file-loader (logic in the url-loader)
        test: /\.(woff2?|ttf|eot|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192
            }
          }
        ]
      },
    ]
  },

  // Enable routing fallback to root
  devServer: {
    historyApiFallback: true
  },

  // enable sourcemaps. Use 'sourcemap' for cleaner sources
  devtool: 'sourcemap',
};

module.exports = webpackConfig;

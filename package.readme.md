# React Components

A suite of Redux powered React UI components.

## Install

```
npm install --save @jhohman/react-components
```

## Live Demo

* http://jhohman-react-components.surge.sh/

## Usage

A brief summary of usage. See the demo site for more information.

```jsx harmony
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

// Minimum tree styles
import '@jhohman/react-components/css/Tree.min.css';

import { 
  Tree,  // Component
  treeReducer,  // Reducer is always component name + Reducer.
  TREE_EXPANDED,  // You must use this constant for expanded
  TREE_COLLAPSED   // You must use this constant for collapsed
} from '@jhohman/react-components/Tree.min';

const store = createStore(treeReducer);

const tree = {
  id: 'root',
  label: 'root',
  children: [  // Required at root.
    {
      id: 'element1',
      label: ...,
      children: [  // Optional, everywhere else
        {
          id: 'element1.1',
          label: ...,
          children: ...
        }
      ]
    },
    ...
    {
      id: 'elementN',
      ...
    }
  ]
};

render(
  <Provider store={store}>
    <Tree
      name="myTree"
      selectCallback={selectCallback}
      tree={tree} />
  </Provider>,
  document.body
);
```

## Repos

View the repos at the following links:

* [react-components](https://bitbucket.org/hohmanjl/react-components)
* [react-components-demo](https://bitbucket.org/hohmanjl/react-components-demo)

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const NoEmitOnErrorsPlugin = require('webpack/lib/NoEmitOnErrorsPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const extractTextAllComponentStyles = new ExtractTextPlugin(`css/all-components.css`);
const extractTextComponentStyles = new ExtractTextPlugin(`css/[name].css`);

const DefinePlugin = require('webpack/lib/DefinePlugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

// Production webpack config
const webpackConfig = (env) => {
  env = env === undefined ? {} : env;
  const production = env.production !== 'false';
  const debug = env.debug === 'true';

  if (debug) {
    console.log('***** WARNING: BUILDING IN DEBUG MODE! *****');
  }

  if (!production) {
    console.log('***** WARNING: BUILDING IN DEVELOPMENT MODE! *****');
  }

  const components = [
    'Input',
    'Loader',
    'Select',
    'StoreView',
    'Tree',
  ];

  const config = {
    entry: {
      index: './src/index.js'
    },

    resolve: {
      modules: ['node_modules', 'src'],
      extensions: ['.js']
    },

    // Exclude external stuff from bundles
    externals: {
      bootstrap: 'Bootstrap',
      react: {
        root: 'React',
        commonjs2: 'react',
        commonjs: 'react',
        amd: 'react',
        umd: 'react',
      },
      'react-dom': {
        root: 'ReactDOM',
        commonjs2: 'react-dom',
        commonjs: 'react-dom',
        amd: 'react-dom',
        umd: 'react-dom',
      },
    },

    output: {
      path: path.join(__dirname, 'build'),
      filename: '[name].js',
      libraryTarget: 'umd',
      library: 'react-components',
      publicPath: '/'
    },

    plugins: [
      extractTextComponentStyles,
      extractTextAllComponentStyles,

      // Setting this to production disables prop-type checking
      // that isn't very useful for downstream consumers.
      new DefinePlugin({
        'process.env.NODE_ENV': production ? "'production'" : "'development'"
      }),

      new NoEmitOnErrorsPlugin(),

      new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.min\.css$/g,
        cssProcessor: require('cssnano'),
        cssProcessorOptions: {discardComments: {removeAll: true}},
        canPrint: true
      }),

      new CopyWebpackPlugin([
        'package.json',
        {from: 'package.readme.md', to: 'readme.md'}
      ])
    ],

    module: {
      rules: [
        {
          // compile the less via less-loader, pass result to css-loader
          // use sourcemaps for easy debugging
          test: /\.less$/,
          use: extractTextComponentStyles.extract({
            use: [
              {
                loader: 'css-loader',
                options: {
                  sourceMap: true
                }
              },
              {
                loader: 'less-loader',
                options: {
                  sourceMap: true
                }
              }
            ]
          })
        },
        {
          enforce: "pre",
          test: /\.js$/,
          exclude: [
            /node_modules/
          ],
          use: [
            {
              // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
              loader: "source-map-loader"
            },
            {
              // compile javascript via eslint loader then pass result to babel loader
              loader: 'eslint-loader',
              options: {
                configFile: '.eslintrc.dev.js'
              }
            },
          ]
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
            cacheDirectory: true,
          },
        },
      ]
    }
  };

  Object.keys(config.entry).map((k) => {
    config.entry[`${k}.min`] = config.entry[k];
  });

  if (debug) {
    config.devtool = 'sourcemap';
  } else {
    // Uglify Source
    config.plugins.push(
      new UglifyJSPlugin({
        include: /\.min\.js$/,
        uglifyOptions: {
          mangle: true,
          compress: true
        }
      })
    );
  }

  if (production) {
    // ensure sourcemaps disabled
    if (config.devtool) {
      throw new Error('Sourcemaps forbidden in production!');
    }
  }

  components.forEach((component) => {
    const entryPoint = `./src/components/${component}/index.js`;

    config.entry[component] = entryPoint;
    config.entry[`${component}.min`] = entryPoint;
  });

  return config;
};


module.exports = webpackConfig;

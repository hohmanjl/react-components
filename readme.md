# React Components

| Item | Description |
| - | - |
| Author | James Hohman |
| Email | jhohman@protonmail.com |
| Package | https://www.npmjs.com/package/@jhohman/react-components |
| Repo | https://bitbucket.org/hohmanjl/react-components/ |
| Demo | http://jhohman-react-components.surge.sh/ |


## Description

A suite of Redux powered React UI components.

## Development

Clone this repo, [react-components](https://bitbucket.org/hohmanjl/react-components).

Clone the [react-components-demo](https://bitbucket.org/hohmanjl/react-components-demo) app.

```
$ cd react-components
$ sudo npm link
$ cd ../react-components-demo
$ npm link @hohmanjl/react-components 
```

Now you can build the distribution.

```
$ cd react-components
$ npm run build
```

Then test it out on the demo site.

```
$ cd react-components-demo
$ npm start
```

## Publishing

To publish the latest to `npm` perform the following.

1. Lint. Test. Build.

   ```
   $ npm run buildtest
   ```

2. Verify the build, prior to publishing. 

   ```
   $ npm pack build/
   $ tar -tf jhohman-react-components-x.x.x.tgz
   ```

3. When satisfied, publish to `npm`.

   ```
   $ npm publish build/ --access public
   ```

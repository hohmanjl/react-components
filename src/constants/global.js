const APP_ROOT_ID = 'reactApp';
const STORE_BINDING = '_store';

export {
  APP_ROOT_ID,
  STORE_BINDING,
};

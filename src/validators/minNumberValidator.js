function minNumberValidator(
  minValue,
  props,
  propName,
  componentName) {

  const value = props[propName];

  if (typeof value !== 'number') {
    return new Error(
      `${componentName}: ${propName} must be a number.`
    );
  }

  if (!isFinite(value)) {
    return new Error(
      `${componentName}: ${propName} must be a valid number.`
    );
  }

  if (props[propName] < minValue) {
    return new Error(
      `${componentName}: ${propName} must be greater than or equal to -1 (root).`
    );
  }
}

export default minNumberValidator;

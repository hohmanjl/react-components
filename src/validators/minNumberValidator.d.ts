export type minNumberValidatorFunc = (
  minValue: number,
  props: any,
  propName: string,
  componentName: string,
) => undefined;

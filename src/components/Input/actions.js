import { inputSetValue } from './actionTypes';

export const inputSetValueAction = (name, value) => {
  return {
    type: inputSetValue,
    name,
    value,
  };
};

import { inputSetValue } from './actionTypes';

const inputReducer = (state = {}, action) => {
  switch (action.type) {
    case inputSetValue:
      return {
        ...state,
        [action.name]: action.value,
      };
    default:
      return state;
  }
};

export default inputReducer;

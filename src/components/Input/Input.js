import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';

import { STORE_BINDING } from 'constants/global';

import StoreComponent from 'components/StoreComponent';

import { inputSetValueAction } from './actions';


class Input extends StoreComponent {
  constructor(props) {
    super(props);

    const componentName = 'Input';
    const { name, value, setValue, timeout } = this.props;
    const stateValue = value ? value : '';

    if (!name) {
      this.requiredProperty('name', componentName);
    }

    this.state = {
      value: stateValue
    };
    setValue(name, stateValue);

    this.handleChange = this.handleChange.bind(this);
    this.debouncedChange = debounce(this.changedValue, timeout);
  }

  changedValue(value) {
    const { name, inputCallback, setValue } = this.props;

    setValue(name, value);
    inputCallback(value);
  }

  handleChange(event) {
    const { target } = event;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    this.setState({ value: value }, () => {
      this.debouncedChange(value);
    });
  }

  render() {
    const props = this.props;
    const { value } = this.state;

    const cleanProps = {};
    const exclude = [
      STORE_BINDING,
      'inputCallback',
      'setValue',
      'storePath',
      'timeout',
    ];
    Object.keys(props).forEach((p) => {
      if (exclude.includes(p)) {
        return;
      }

      cleanProps[p] = props[p];
    });

    return (
      <input
        {...cleanProps}
        value={this.state.value}
        checked={ props.type === 'checkbox' && value }
        onChange={this.handleChange} />
    );
  }
}

Input.propTypes = {
  ...StoreComponent.propTypes,
  inputCallback: PropTypes.func,
  value: PropTypes.string,
  timeout: PropTypes.number,
  type: PropTypes.string,
};

Input.defaultProps = {
  inputCallback: () => {},
  timeout: 100,
  type: 'text',
};

const mapStateToProps = state => {
  return { [STORE_BINDING]: state };
};

const mapDispatchToProps = dispatch => {
  return {
    setValue: (name, value) => {
      dispatch(inputSetValueAction(name, value));
    }
  };
};

const BoundInput = connect(
  mapStateToProps,
  mapDispatchToProps
)(Input);

export { Input };
export default BoundInput;

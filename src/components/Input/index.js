import Input from './Input';
import inputReducer from './reducers';

export default Input;
export {
  Input,
  inputReducer,
};

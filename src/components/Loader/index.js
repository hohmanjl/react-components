import BoundLoader, { Loader } from './Loader';
import { SMALL, MEDIUM, LARGE } from './constants';

export default BoundLoader;
export {
  BoundLoader,
  Loader,
  SMALL,
  MEDIUM,
  LARGE,
};

const SMALL = 15;
const MEDIUM = 25;
const LARGE = 35;
const BORDER_PROPORTION = 0.1;

export {
  SMALL,
  MEDIUM,
  LARGE,
  BORDER_PROPORTION,
};

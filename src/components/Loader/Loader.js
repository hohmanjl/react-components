import * as PropTypes from 'prop-types';
import * as React from 'react';
// import { connect } from 'react-redux';
// import get from 'lodash/get';

// import { STORE_BINDING } from 'constants/global';

import { SMALL, MEDIUM, LARGE, BORDER_PROPORTION } from './constants';

import './styles.less';

function Loader(props) {
  const {
    className,
    id,
    message,
    show,
    size,
    // storePath,
    twin,
  } = props;

  // * 1 to address bug, otherwise size wont update
  // Don't know if it's transpiler or react bug.
  const _size = size * 1;

  // override twin setting at smaller scales
  const forcedTwin = _size > SMALL ? twin : true;
  // const fullStorePath = [
  //   STORE_BINDING,
  //   storePath
  // ].filter(a => a).join('.');
  // const storeValue = get(props, fullStorePath);
  const classes = [
    'loader',
    className
  ].join(' ');

  const loaderWrapperStyle = {
    display: show ? undefined : 'none',
  };

  const innerStyle = {
    borderWidth: Math.max(Math.ceil(_size * BORDER_PROPORTION), 2),
    borderBottomColor: forcedTwin ? undefined : 'transparent',
  };

  const styles = {
    height: _size,
    width: _size,
    animationDuration: _size <= SMALL ? '1s' : undefined,
    ...innerStyle,
  };

  const spinnerContainerStyles = {
    height: _size,
    width: _size,
  };

  return (
    <div
      id={id}
      className={classes}
      style={loaderWrapperStyle}>
      <div
        className="spinner"
        style={spinnerContainerStyles}>
        <div
          className="l1"
          style={styles} >
          {size > SMALL &&
          <div className="l2" style={innerStyle}>
            {size > MEDIUM &&
            <div className="l3" style={innerStyle}/>
            }
          </div>
          }
        </div>
      </div>
      {message &&
      <div className="loader-message">
        {message}
      </div>
      }
    </div>
  );
}


Loader.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  message: PropTypes.string,
  show: PropTypes.bool.isRequired,
  size: PropTypes.number,
  storePath: PropTypes.string,
  twin: PropTypes.bool,
};

Loader.defaultProps = {
  size: LARGE,
  twin: false,
};

// const mapStateToProps = state => {
//   return { [STORE_BINDING]: state };
// };

function BoundLoader() {
  throw new Error(
    'Store interface not implemented for this component. '
    + 'Import the "dumb" Loader and use props.'
  );

  // return connect(
  //   mapStateToProps
  // )(Loader);
}

export { Loader };
export default BoundLoader;

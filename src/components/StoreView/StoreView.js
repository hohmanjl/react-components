import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';
import get from 'lodash/get';

import { STORE_BINDING } from 'constants/global';

function StoreView(props) {
  const { className, id, formatter, storePath } = props;
  const fullStorePath = [
    STORE_BINDING,
    storePath
  ].filter(a => a).join('.');
  const storeValue = get(props, fullStorePath);
  const classes = [
    'store-view',
    className
  ].join(' ');
  const formatted = formatter(storeValue);

  return (
    <div
      className={classes}
      id={id}>
      <pre>
        <code>
          {JSON.stringify(formatted, null, 2)}
        </code>
      </pre>
    </div>
  );
}

StoreView.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  formatter: PropTypes.func,
  storePath: PropTypes.string.isRequired,
};

StoreView.defaultProps = {
  formatter: val => val,
};

const mapStateToProps = state => {
  return { [STORE_BINDING]: state };
};

const BoundStoreView = connect(
  mapStateToProps
)(StoreView);

export { StoreView };
export default BoundStoreView;

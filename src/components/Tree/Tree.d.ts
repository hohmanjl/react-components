import * as React from 'react';

export interface TreeProps {
  tree: NodeData;
}

declare const Tree: React.ComponentType<TreeProps>;

export default Tree;

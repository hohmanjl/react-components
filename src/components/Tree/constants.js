const rootLevel = -1;
const TREE_COLLAPSED = Symbol('tree:collapsed');
const TREE_EXPANDED = Symbol('tree:expanded');

export {
  rootLevel,
  TREE_COLLAPSED,
  TREE_EXPANDED,
};

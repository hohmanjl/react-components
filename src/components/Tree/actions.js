import {
  bulkSetExpandedActionType,
  multiSetSelectedActionType,
  setExpandedActionType,
  singleSetSelectedActionType,
} from './actionTypes';

export const treeExpandAction = (name, node, state = undefined) => {
  return {
    type: setExpandedActionType,
    name,
    node,
    state,
  };
};

export const treeBulkExpandAction = (name, nodes) => {
  return {
    type: bulkSetExpandedActionType,
    name,
    nodes,
  };
};

export const treeSingleSelectAction = (name, node) => {
  return {
    type: singleSetSelectedActionType,
    name,
    node,
  };
};

export const treeMultiSelectAction = (name, node) => {
  return {
    type: multiSetSelectedActionType,
    name,
    node,
  };
};

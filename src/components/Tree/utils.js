function treeFilter(node, filterFn) {
  const displayed = filterFn(node);

  if (displayed) {
    return Object.assign(
      {},
      node
    );
  }

  const displayedChildren = [];
  node.children && node.children.forEach((childNode) => {
    const child = treeFilter(childNode, filterFn);

    if (child) {
      displayedChildren.push(child);
    }
  });

  if (displayedChildren.length === 0) {
    return undefined;
  }

  return Object.assign(
    {},
    node,
    { children: displayedChildren }
  );
}

function traverseFlat(node, fn, parent = null, depth = 0) {
  const children = node.children || [];
  let results = [fn(node, parent, depth)];

  children.map((childNode) => {
    results = results.concat(
      traverseFlat(childNode, fn, node, depth + 1)
    );
  });

  return results;
}

export {
  traverseFlat,
  treeFilter,
};

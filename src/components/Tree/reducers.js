import {
  bulkSetExpandedActionType,
  multiSetSelectedActionType,
  setExpandedActionType,
  singleSetSelectedActionType,
} from './actionTypes';

function bookChildren(target, state, node) {
  const { children } = node;
  if (children) {
    children.forEach((childNode) => {
      target = bookChildren(target, state, childNode);
    });
  }

  target[node.id] = state;

  return target;
}

const multiSelected = (state, action) => {
  const selected = {};
  const componentState = state[action.name] || {};
  const stateSelected = componentState.selected || {};
  const bookState = stateSelected[action.node.id] ? undefined : true;
  const newState = bookChildren({}, bookState, action.node);
  const allSelected = Object.assign(
    {},
    stateSelected,
    newState
  );

  Object.keys(allSelected)
    .filter((key) => {
      return allSelected[key];
    })
    .map((key) => {
      selected[key] = true;
    });

  return {
    ...state,
    [action.name]: {
      ...componentState,
      selected,
    },
  };
};

const singleSelected = (state, action) => {
  const selected = {};
  const componentState = state[action.name] || {};
  const stateSelected = componentState.selected || {};
  const bookState = stateSelected[action.node.id] ? undefined : true;
  const allSelected = {
    [action.node.id]: bookState
  };

  Object.keys(allSelected)
    .filter((key) => {
      return allSelected[key];
    })
    .map((key) => {
      selected[key] = true;
    });

  return {
    ...state,
    [action.name]: {
      ...componentState,
      selected,
    },
  };
};

const setExpanded = (state, action) => {
  const componentState = state[action.name] || {};
  const expanded = componentState.expanded || {};

  if (action.state === undefined) {
    expanded[action.node.id] = !expanded[action.node.id];
  } else {
    expanded[action.node.id] = action.state;
  }

  return {
    ...state,
    [action.name]: {
      ...componentState,
      expanded,
    },
  };
};

const setBulkExpanded = (state, action) => {
  const componentState = state[action.name] || {};
  const expanded = componentState.expanded || {};

  Object.assign(expanded, action.nodes);

  return {
    ...state,
    [action.name]: {
      ...componentState,
      expanded,
    },
  };
};

const treeReducer = (state = {}, action) => {
  switch (action.type) {
    case singleSetSelectedActionType:
      return singleSelected(state, action);
    case multiSetSelectedActionType:
      return multiSelected(state, action);
    case setExpandedActionType:
      return setExpanded(state, action);
    case bulkSetExpandedActionType:
      return setBulkExpanded(state, action);
    default:
      return state;
  }
};

export default treeReducer;

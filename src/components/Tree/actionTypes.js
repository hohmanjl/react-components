const setExpandedActionType = 'tree:set-expanded';
const bulkSetExpandedActionType = 'tree:bulk-set-expanded';
const singleSetSelectedActionType = 'tree:single-set-selected';
const multiSetSelectedActionType = 'tree:multi-set-selected';

export {
  bulkSetExpandedActionType,
  multiSetSelectedActionType,
  setExpandedActionType,
  singleSetSelectedActionType,
};

interface NodeData {
  id: string;
  label: string;
  children?: object;
}

import React from 'react';
import { mount, shallow } from 'enzyme';
import { createRenderer } from 'react-dom/test-utils';

import { Tree } from './Tree';

const tree = {
  id: 'root',
  label: 'root',
  children: [
    {
      id: 'usa',
      label: 'United States',
    },
  ],
};

describe('<Tree />', () => {
  describe('shallow render', () => {
    it('renders', () => {
      expect(
        createRenderer().render(
          <Tree
            clickBulkSetExpanded={() => {}}
            name="testTree"
            tree={ tree } />
        )
      ).toMatchSnapshot();
    });
  });

  describe('required props', () => {
    it('when it is missing required prop "name" it raises exception', () => {
      // Disable the propType warning
      jest.spyOn(console, 'error')
        .mockImplementation(() => {});

      const requiredPropertySpy = jest
        .spyOn(Tree.prototype, 'requiredProperty');
      const wrapper = () => {
        shallow(
          <Tree
            clickBulkSetExpanded={() => {}}
            tree={ tree } />
        );
      };

      expect(wrapper).toThrow();
      expect(requiredPropertySpy).toHaveBeenCalledWith('name', 'Tree');
      Tree.prototype.requiredProperty.mockRestore();
    });

    it('when it is missing required prop "tree" it raises exception', () => {
      // Disable the propType warning
      jest.spyOn(console, 'error')
        .mockImplementation(() => {});

      const requiredPropertySpy = jest
        .spyOn(Tree.prototype, 'requiredProperty');
      const wrapper = () => {
        shallow(
          <Tree
            clickBulkSetExpanded={() => { }}
            name="bob" />
        );
      };

      expect(wrapper).toThrow();
      expect(requiredPropertySpy).toHaveBeenCalledWith('tree', 'Tree');
      Tree.prototype.requiredProperty.mockRestore();
    });
  });

  describe('constructor', () => {
    it('initTree is called', () => {
      const initTree = jest
        .spyOn(Tree.prototype, 'initTree')
        .mockImplementation(() => { return {}; });

      const wrapper = shallow(
        <Tree
          clickBulkSetExpanded={() => { }}
          tree={tree}
          name="bob" />
      );

      expect(wrapper.length).toBeGreaterThan(0);
      expect(initTree).toHaveBeenCalledWith();
      expect(initTree).toHaveBeenCalled();
      Tree.prototype.initTree.mockRestore();
    });

    it('initExpandedState is called', () => {
      const initExpandedState = jest
        .spyOn(Tree.prototype, 'initExpandedState')
        .mockImplementation(() => { return {}; });

      const wrapper = shallow(
        <Tree
          clickBulkSetExpanded={() => { }}
          tree={tree}
          name="bob" />
      );

      expect(wrapper.length).toBeGreaterThan(0);
      expect(initExpandedState).toHaveBeenCalledWith();
      expect(initExpandedState).toHaveBeenCalled();
      Tree.prototype.initExpandedState.mockRestore();
    });
  });

  describe('full render', () => {
    it('full renders', () => {
      const wrapper = mount(
        <Tree
        tree={tree}
        name="bob"
        clickBulkSetExpanded={() => { }} />
      );
      expect(wrapper.length).toBeGreaterThan(0);
    });
  });
});

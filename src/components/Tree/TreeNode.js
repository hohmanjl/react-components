import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';

import minNumberValidator from 'validators/minNumberValidator';

import { STORE_BINDING } from 'constants/global';

import StoreComponent from 'components/StoreComponent';

import {
  treeMultiSelectAction,
  treeExpandAction,
  treeSingleSelectAction,
} from './actions';
import { rootLevel } from './constants';


class TreeNode extends StoreComponent {
  constructor(props) {
    super(props);

    const { node, parentSelectEnabled } = props;
    const { children } = node;

    this.state = {
      clickSelectEnabled: parentSelectEnabled ? true : !children
    };

    this.clickHeader = this.clickHeader.bind(this);
    this.clickArrow = this.clickArrow.bind(this);
  }

  componentDidMount() {
    const { elementRefs, node } = this.props;

    elementRefs[node.path] = this.refs.header;
  }

  clickHeader() {
    const {
      name,
      node,
      clickSetSingleSelected,
      clickSetMultiSelected,
      multiSelectEnabled,
      selectCallback
    } = this.props;
    const { clickSelectEnabled } = this.state;

    if (clickSelectEnabled) {
      selectCallback(node);
      multiSelectEnabled
        ? clickSetMultiSelected(name, node)
        : clickSetSingleSelected(name, node);
    }
  }

  clickArrow(event) {
    event.stopPropagation();

    const { clickSetExpanded, name, node } = this.props;

    clickSetExpanded(name, node);
  }

  genChildren() {
    const {
      elementRefs,
      level,
      multiSelectEnabled,
      name,
      node,
      parentSelectEnabled,
      selectCallback,
      showSelectIndicator,
      store,
      storePath,
    } = this.props;
    const { children } = node;

    return children
      .map((childNode) => {
        return <BoundTreeNode
          elementRefs={elementRefs}
          key={childNode.id}
          level={level + 1}
          multiSelectEnabled={multiSelectEnabled}
          name={name}
          node={childNode}
          parentSelectEnabled={parentSelectEnabled}
          selectCallback={selectCallback}
          showSelectIndicator={showSelectIndicator}
          store={store}
          storePath={storePath} />;
      });
  }

  renderChildren() {
    const { className, id, level, node } = this.props;
    const { children } = node;

    if (!children) {
      return null;
    }

    const childClassWrapper = level === rootLevel
      ? ['react-tree-component', className].join(' ')
      : 'tree-children';

    const childId = level === rootLevel ? id : undefined;

    return (
      <div id={childId} className={childClassWrapper}>
        {this.genChildren()}
      </div>
    );
  }

  renderSelectIndicator() {
    const {
      node,
      showSelectIndicator,
      multiSelectEnabled,
    } = this.props;
    const configuredStore = this.getConfiguredStore();
    const selectedStore = configuredStore.selected || {};
    const selected = selectedStore[node.id];

    if (
      !selected
      || !showSelectIndicator
      || multiSelectEnabled) {
      return null;
    }

    return <span className="tree-selected" />;
  }

  renderMultiSelectIndicator() {
    const {
      showSelectIndicator,
      multiSelectEnabled,
    } = this.props;
    const { clickSelectEnabled } = this.state;

    if (!showSelectIndicator || !multiSelectEnabled) {
      return null;
    }

    const classes = [
      'btn-xs tree-select-indicator',
      clickSelectEnabled ? undefined : 'rc-disabled',
    ].join(' ');

    return <span className={classes} />;
  }

  render() {
    const { level, node } = this.props;
    const { clickSelectEnabled } = this.state;
    const { children, label } = node;
    const configuredStore = this.getConfiguredStore();
    const selectedStore = configuredStore.selected || {};
    const expandedStore = configuredStore.expanded || {};
    const expanded = expandedStore[node.id];
    const selected = selectedStore[node.id];

    if (level === rootLevel) {
      return this.renderChildren();
    }

    const wrapperClasses = [
      `tree-level-${level}`,
      expanded ? 'expanded' : undefined,
    ].join(' ');

    const indicatorArrowClasses = [
      'btn-xs indicator-arrow clickable',
      children ? undefined : 'rc-hidden',
    ].join(' ');

    const headerClasses = [
      'tree-header no-select',
      clickSelectEnabled ? 'clickable' : undefined,
      selected ? 'selected' : undefined
    ].join(' ');

    return (
      <div className={wrapperClasses}>
        <div
          data-path={node.path}
          className={headerClasses}
          tabIndex={0}
          ref="header"
          onClick={this.clickHeader}>
          <div
            data-path={node.path}
            tabIndex={0}
            onClick={this.clickArrow}
            className={indicatorArrowClasses} />
          {this.renderMultiSelectIndicator()}
          <span className="tree-label">{label}</span>
          {this.renderSelectIndicator()}
        </div>
        {this.renderChildren()}
      </div>
    );
  }
}

const nodeShape = PropTypes.shape({
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.symbol
  ]).isRequired,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
});
nodeShape.children = PropTypes.arrayOf(nodeShape);

TreeNode.propTypes = {
  ...StoreComponent.propTypes,
  level: minNumberValidator.bind(null, rootLevel),
  multiSelectEnabled: PropTypes.bool,
  node: nodeShape.isRequired,
  parentSelectEnabled: PropTypes.bool,
  selectCallback: PropTypes.func,
  showSelectIndicator: PropTypes.bool,
};

const mapStateToProps = state => {
  return { [STORE_BINDING]: state };
};

const mapDispatchToProps = dispatch => {
  return {
    clickSetExpanded: (name, node) => {
      dispatch(treeExpandAction(name, node));
    },
    clickSetSingleSelected: (name, node) => {
      dispatch(treeSingleSelectAction(name, node));
    },
    clickSetMultiSelected: (name, node) => {
      dispatch(treeMultiSelectAction(name, node));
    }
  };
};

const BoundTreeNode = connect(
  mapStateToProps,
  mapDispatchToProps
)(TreeNode);

export { TreeNode };
export default BoundTreeNode;

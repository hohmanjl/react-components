import * as React from 'react';

export interface TreeNodeProps {
    level: number;
    node: NodeData;
}

declare const TreeNode: React.ComponentType<TreeNodeProps>;

export default TreeNode;

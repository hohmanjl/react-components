import Tree from './Tree';
import treeReducer from './reducers';
import { TREE_COLLAPSED, TREE_EXPANDED } from './constants';

export default Tree;
export {
  Tree,
  treeReducer,
  TREE_COLLAPSED,
  TREE_EXPANDED
};

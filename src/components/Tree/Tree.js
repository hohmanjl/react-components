import PropTypes from 'prop-types';
import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import cloneDeep from 'lodash/cloneDeep';

import {
  KEY_ARROW_UP,
  KEY_ARROW_DOWN,
  KEY_ARROW_LEFT,
  KEY_ARROW_RIGHT,
  KEY_ENTER,
  KEY_SPACE,
} from 'constants/keycodes';
import { STORE_BINDING } from 'constants/global';
import getEventTarget from 'utils/getEventTarget';

import StoreComponent from 'components/StoreComponent';

import {treeBulkExpandAction, treeExpandAction} from './actions';
import {
  rootLevel,
  TREE_COLLAPSED,
  TREE_EXPANDED,
} from './constants';
import TreeNode from './TreeNode';
import { traverseFlat, treeFilter } from './utils';

import 'assets/less/global.less';
import './styles.less';


class Tree extends StoreComponent {
  constructor(props) {
    super(props);

    const componentName = 'Tree';
    const { initialState, name, tree } = props;

    if (!tree) {
      this.requiredProperty('tree', componentName);
    }

    if (!name) {
      this.requiredProperty('name', componentName);
    }

    const targetPath = typeof initialState === 'string'
      ? initialState
      : '';
    const treeInitialState = initialState === TREE_EXPANDED
      ? TREE_EXPANDED
      : TREE_COLLAPSED;

    this.state = {
      annotatedTree: this.initTree(),
      targetPath,
      treeInitialState,
      filteredTree: {},
      elementRefs: {},
    };

    this.initExpandedState();
  }

  componentWillReceiveProps(nextProps) {
    const { annotatedTree } = this.state;
    const { filter, sort } = nextProps;

    const filteredTree = treeFilter(annotatedTree, filter) || {};
    traverseFlat(filteredTree, this.sortChildren.bind(null, sort));

    this.setState({ filteredTree });
  }

  annotateTree(node, parent, depth) {
    if (parent && depth > 1) {
      node.path = [parent.path, node.id].join('.');
    } else {
      node.path = node.id;
    }
  }

  sortChildren(sortFn, node) {
    const { children } = node;
    if (children) {
      children.sort(sortFn);
    }
  }

  initExpandedState() {
    const { clickBulkSetExpanded, name } = this.props;
    const {
      targetPath,
      treeInitialState,
      annotatedTree
    } = this.state;
    const nodes = {};
    const expandableNodes = traverseFlat(annotatedTree, this.expandableFilter)
      .filter(node => node);

    if (targetPath) {
      expandableNodes.reduce(
        (acc, node) => {
          acc[node.id] = targetPath.startsWith(node.path);

          return acc;
        },
        nodes
      );
    } else {
      const state = treeInitialState === TREE_EXPANDED;

      expandableNodes.reduce(
        (acc, node) => {
          acc[node.id] = state;

          return acc;
        },
        nodes
      );
    }

    return clickBulkSetExpanded(name, nodes);
  }

  initTree() {
    const { tree } = this.props;
    const annotatedTree = cloneDeep(tree);
    traverseFlat(annotatedTree, this.annotateTree);

    return annotatedTree;
  }

  expandableFilter(node) {
    const { children } = node;

    if (!children) {
      return;
    }

    return node;
  }

  getNode(node, path) {
    let foundNode;

    const { children } = node;
    if (children) {
      children.some((childNode) => {
        if (path.startsWith(childNode.path)) {
          foundNode = this.getNode(childNode, path);

          return true;
        }
      });
    }

    if (path === node.path) {
      return node;
    }

    return foundNode;
  }

  expandCollapse(path, expand) {
    const configuredStore = this.getConfiguredStore();
    const expandedStore = configuredStore.expanded;
    const { filteredTree } = this.state;
    const { clickSetExpanded, name } = this.props;
    const node = this.getNode(filteredTree, path);

    if (!node.children) {
      return;
    }

    if (expandedStore[node.id] === expand) {
      return;
    }

    clickSetExpanded(name, node, expand);
  }

  move(path, previous) {
    const elems = Array
      .from(document.getElementsByClassName('tree-header'))
      .filter(el => el.offsetParent);
    const { elementRefs } = this.state;
    const elemsLength = elems.length;

    if (previous) {
      elems.reverse();
    }

    let currentIndex = null;

    for (let i = 0; i < elemsLength; i++) {
      const el = elems[i];
      const elPath = el.dataset.path;

      if (path === elPath) {
        currentIndex = i;
        continue;
      }

      if (currentIndex === null) {
        continue;
      }

      return elementRefs[elPath];
    }

    const el = elems[0];
    const elPath = el.dataset.path;
    return elementRefs[elPath];
  }

  handleKeyPress(e) {
    const senderElem = getEventTarget(e);
    const { path } = senderElem.dataset;
    const { keyCode } = e;

    let target;

    switch (keyCode) {
      case KEY_ARROW_UP:
        target = this.move(path, true);
        ReactDOM.findDOMNode(target).focus();
        break;
      case KEY_ARROW_DOWN:
        target = this.move(path, false);
        ReactDOM.findDOMNode(target).focus();
        break;
      case KEY_ARROW_LEFT:
        this.expandCollapse(path, false);
        break;
      case KEY_ARROW_RIGHT:
        this.expandCollapse(path, true);
        break;
      case KEY_SPACE:
        ReactDOM.findDOMNode(senderElem).click();
        break;
      case KEY_ENTER:
        ReactDOM.findDOMNode(senderElem).click();
        break;
      default:
        return;
    }

    e.preventDefault();
    e.stopPropagation();
  }

  render() {
    const {
      className,
      id,
      multiSelectEnabled,
      name,
      parentSelectEnabled,
      showSelectIndicator,
      selectCallback,
      store,
      storePath,
    } = this.props;
    const {
      elementRefs,
      filteredTree,
      treeInitialState,
    } = this.state;

    if (!filteredTree.children) {
      const classes = [
        'react-tree-component',
        className
      ].join(' ');
      return (
        <div
          id={id}
          className={classes}>
          <div className="tree-empty">
            <p>No results after filtering.</p>
          </div>
        </div>
      );
    }

    return (
      <div
        onKeyDown={this.handleKeyPress.bind(this)}>
        <TreeNode
          className={className}
          elementRefs={elementRefs}
          id={id}
          level={rootLevel}
          multiSelectEnabled={multiSelectEnabled}
          name={name}
          node={filteredTree}
          initialState={treeInitialState}
          parentSelectEnabled={parentSelectEnabled}
          selectCallback={selectCallback}
          showSelectIndicator={showSelectIndicator}
          store={store}
          storePath={storePath} />
      </div>
    );
  }
}

Tree.propTypes = {
  ...StoreComponent.propTypes,
  filter: PropTypes.func,
  initialState: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.symbol
  ]),
  multiSelectEnabled: PropTypes.bool,
  parentSelectEnabled: PropTypes.bool,
  selectCallback: PropTypes.func,
  showSelectIndicator: PropTypes.bool,
  sort: PropTypes.func,
  tree: PropTypes.object.isRequired,
};

Tree.defaultProps = {
  filter: () => true,
  initialState: TREE_COLLAPSED,
  multiSelectEnabled: true,
  parentSelectEnabled: true,
  selectCallback: () => {},
  showSelectIndicator: true,
  sort: () => 0,
};

const mapStateToProps = state => {
  return { [STORE_BINDING]: state };
};

const mapDispatchToProps = dispatch => {
  return {
    clickSetExpanded: (name, node, state) => {
      dispatch(treeExpandAction(name, node, state));
    },
    clickBulkSetExpanded: (name, nodes) => {
      dispatch(treeBulkExpandAction(name, nodes));
    },
  };
};

const BoundTree = connect(
  mapStateToProps,
  mapDispatchToProps
)(Tree);

export default BoundTree;
export { Tree };

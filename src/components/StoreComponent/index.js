import * as PropTypes from 'prop-types';
import * as React from 'react';
import get from 'lodash/get';

import { STORE_BINDING } from 'constants/global';


class StoreComponent extends React.PureComponent {
  formatPropValue(val) {
    if (val === undefined) {
      return 'undefined';
    }

    if (val === null) {
      return 'null';
    }

    return val;
  }

  requiredProperty(propName, componentName = '') {
    const compName = componentName || this.constructor.name;
    const prop = this.props[propName];

    if (!prop) {
      const propValue = this.formatPropValue(prop);
      throw new Error(
        `The prop "${propName}" is marked as required `
        + `in "${compName}", but its value is "${propValue}".`
      );
    }
  }

  getConfiguredStore() {
    const { props } = this;
    const { name, storePath } = props;
    const fullStorePath = [
      STORE_BINDING,
      storePath
    ].filter(a => a).join('.');
    const configuredStore = get(props, fullStorePath);

    if (!configuredStore) {
      const componentName = this.constructor.name;

      // eslint-disable-next-line no-console
      console.log(`Look for store in the ${componentName} props object.`, props);
      throw new Error(`${componentName}: Could not retrieve store at "${fullStorePath}".`);
    }

    return configuredStore[name] || {};
  }
}

StoreComponent.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  store: PropTypes.object,
  storePath: PropTypes.string,
};

export default StoreComponent;

import { selectSetSelected } from './actionTypes';

const selectReducer = (state = {}, action) => {
  switch (action.type) {
    case selectSetSelected:
      return {
        ...state,
        [action.name]: action.value
      };
    default:
      return state;
  }
};

export default selectReducer;

import Select from './Select';
import selectReducer from './reducers';

export default Select;
export {
  Select,
  selectReducer,
};

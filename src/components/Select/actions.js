import { selectSetSelected } from './actionTypes';

export const selectSetSelectAction = (name, value) => {
  return {
    type: selectSetSelected,
    name,
    value,
  };
};

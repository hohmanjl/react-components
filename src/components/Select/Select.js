import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';

import { STORE_BINDING } from 'constants/global';

import StoreComponent from 'components/StoreComponent';

import { selectSetSelectAction } from './actions';


class Select extends StoreComponent {
  constructor(props) {
    super(props);

    const componentName = 'Select';
    const { name, selected, setSelected } = this.props;

    if (!name) {
      this.requiredProperty('name', componentName);
    }

    if (selected) {
      setSelected(name, selected);
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const { value } = event.target;
    const { name, selectCallback, setSelected } = this.props;

    setSelected(name, value);
    selectCallback(value);
  }

  renderOptions() {
    const { options } = this.props;

    return options.map((option) => {
      return (
        <option
          key={option.value}
          value={option.value}>
          {option.label}
        </option>
      );
    });
  }

  render() {
    const { className, id, name } = this.props;
    const configuredStore = this.getConfiguredStore();

    return (
      <select
        className={className}
        id={id}
        name={name}
        value={configuredStore}
        onChange={this.handleChange}>
        {this.renderOptions()}
      </select>
    );
  }
}

Select.propTypes = {
  ...StoreComponent.propTypes,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]).isRequired,
    label: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]).isRequired,
  })).isRequired,
  selectCallback: PropTypes.func,
  selected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
};

Select.defaultProps = {
  selectCallback: () => {},
};

const mapStateToProps = state => {
  return { [STORE_BINDING]: state };
};

const mapDispatchToProps = dispatch => {
  return {
    setSelected: (name, value) => {
      dispatch(selectSetSelectAction(name, value));
    }
  };
};

const BoundSelect = connect(
  mapStateToProps,
  mapDispatchToProps
)(Select);

export { Select };
export default BoundSelect;

function getEventTarget(event){
  const evt = event || window.event;

  return evt.target || evt.srcElement;
}

export default getEventTarget;

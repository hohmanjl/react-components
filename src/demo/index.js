import 'bootstrap/dist/css/bootstrap.min.css';
import '../index';

import * as React from 'react';
import * as ReactDom from 'react-dom';

import { APP_ROOT_ID } from 'constants/global';
import DemoTree from './DemoTree';

const rootElement = document.getElementById(APP_ROOT_ID);

ReactDom.render(
  <div className='container'>
    <div className='jumbotron'>
      <h1>React UI Components</h1>
      <p>Development Sandbox</p>
    </div>
    <div>
      <p>
        For the demo site please visit { ' ' }
        <a
          href="https://bitbucket.org/hohmanjl/react-components-demo"
          aria-label="Visit the demo site repository">
          https://bitbucket.org/hohmanjl/react-components-demo
        </a>
      </p>
    </div>
    <div className='page-header'>
      <h1>Tree Component</h1>
    </div>
    <div>
      <DemoTree />
    </div>
  </div>,
  rootElement
);

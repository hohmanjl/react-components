import * as React from 'react';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';

import {
  Tree,
  treeReducer,
  TREE_EXPANDED
} from 'components/Tree';
import { Select, selectReducer } from 'components/Select';
import { Input, inputReducer } from 'components/Input';

import treeDemoNodes from './constants';
import {
  defaultSortOption,
  iContainsFilter,
  sortOptions,
  treeCallback
} from './utils';

import './styles.less';

const reducers = combineReducers({
  input: inputReducer,
  select: selectReducer,
  tree: treeReducer,
});

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__
  && window.__REDUX_DEVTOOLS_EXTENSION__()
);


class DemoTree extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      sortFn: defaultSortOption.fn,
      filterFn: undefined,
    };

    this.inputCallback = this.inputCallback.bind(this);
    this.selectCallback = this.selectCallback.bind(this);
  }

  inputCallback(value) {
    if (value) {
      return this.setState({ filterFn: iContainsFilter.bind(null, value) });
    }

    this.setState({ filterFn: undefined });
  }

  selectCallback(value) {
    const selected = sortOptions.filter((option) => {
      return option.value === value;
    });

    this.setState({ sortFn: selected[0].fn });
  }

  render() {
    const { filterFn, sortFn } = this.state;

    return (
      <Provider store={store}>
        <div>

          <div className="row">
            <div className="col-xs-6">
              <Select
                className="form-control"
                name="sort"
                options={sortOptions}
                selectCallback={this.selectCallback}
                selected={defaultSortOption.value}
                storePath={'select'} />
            </div>

            <div className="col-xs-6">
              <Input
                className="form-control"
                name="filterInput"
                placeholder="filter"
                inputCallback={this.inputCallback}
                storePath={'input'} />
            </div>
          </div>

          <Tree
            filter={filterFn}
            initialState={TREE_EXPANDED}
            name="myTree"
            selectCallback={treeCallback}
            sort={sortFn}
            storePath={'tree'}
            tree={treeDemoNodes} />
        </div>
      </Provider>
    );
  }
}

export default DemoTree;

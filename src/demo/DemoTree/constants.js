const usa = {
  id: 'usa',
  label: 'United States of America',
  children: [
    {
      id: 'ny',
      label: 'New York',
      children: [
        {
          id: 'nyc',
          label: 'New York City',
          children: [
            {
              id: 'queens-county',
              label: 'Queens County',
            },
            {
              id: 'new-york-county',
              label: 'New York County',
            },
            {
              id: 'kings-county',
              label: 'Kings County',
            },
          ],
        },
        {
          id: 'syracuse',
          label: 'Syracuse',
        },
        {
          id: 'albany',
          label: 'Albany',
          children: [
            {
              id: 'albany-county',
              label: 'Albany County',
            },
          ],
        },
      ],
    },
    {
      id: 'ga',
      label: 'Georgia',
      children: [
        {
          id: 'atl',
          label: 'Atlanta',
          children: [
            {
              id: 'fulton-county',
              label: 'Fulton County',
            },
            {
              id: 'forsyth-county',
              label: 'Forsyth County',
            },
          ],
        },
      ],
    },
    {
      id: 'fl',
      label: 'Florida',
    },
  ],
};

const europe = {
  id: 'europe',
  label: 'Europe',
  children: [
    {
      id: 'fr',
      label: 'France',
      children: [
        {
          id: 'paris',
          label: 'Paris',
        },
        {
          id: 'nice',
          label: 'Nice',
        },
      ],
    },
    {
      id: 'it',
      label: 'Italy',
    },
    {
      id: 'germany',
      label: 'Germany',
      children: [
        {
          id: 'berlin',
          label: 'Berlin',
        },
      ],
    },
  ],
};

const asia = {
  id: 'asia',
  label: 'Asia',
};

const treeDemoNodes = {
  id: 'root',
  label: 'root',
  children: [
    usa,
    asia,
    europe,
  ],
};

export default treeDemoNodes;

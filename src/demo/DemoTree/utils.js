function treeCallback(node) {
  // eslint-disable-next-line no-console
  console.log('selected node', node);
}

function propSort(reverse = false, prop, a, b) {
  const dir = reverse === true ? -1 : 1;

  if (a[prop] < b[prop]) {
    return -1 * dir;
  } else if (a[prop] > b[prop]) {
    return 1 * dir;
  }

  return 0;
}

function childSort(reverse = false, a, b) {
  const dir = reverse === true ? -1 : 1;
  const aChildren = a.children && a.children.length || 0;
  const bChildren = b.children && b.children.length || 0;

  if (aChildren < bChildren) {
    return -1 * dir;
  } else if (aChildren > bChildren) {
    return 1 * dir;
  }

  return 0;
}

function iContainsFilter(value, node) {
  const iValue = value.toLowerCase();
  const iLabel = node.label.toLowerCase();

  return iLabel.includes(iValue);
}

const defaultSortOption = {
  value: 'default',
  label: 'Unsorted',
  fn: () => 0,
};

const sortOptions = [
  {
    value: 'lasc',
    label: 'Label Ascending',
    fn: propSort.bind(null, false, 'label'),
  },
  {
    value: 'ldesc',
    label: 'Label Descending',
    fn: propSort.bind(null, true, 'label'),
  },
  {
    value: 'iasc',
    label: 'Id Ascending',
    fn: propSort.bind(null, false, 'id'),
  },
  {
    value: 'idesc',
    label: 'Id Descending',
    fn: propSort.bind(null, true, 'id'),
  },
  {
    value: 'casc',
    label: 'Num Children Ascending',
    fn: childSort.bind(null, false),
  },
  {
    value: 'cdesc',
    label: 'Num Children Descending',
    fn: childSort.bind(null, true),
  },
  defaultSortOption,
];

export {
  treeCallback,
  propSort,
  childSort,
  iContainsFilter,
  sortOptions,
  defaultSortOption,
};
